import argparse
import os
import requests
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

# from bs4 import BeautifulSoup


def main(url: str, path_html: os.PathLike, monitor: bool = True, store: bool = False):

    page = requests.get(url).text

    if store or not os.path.isfile(path_html):
        store_page(page, path_html)

    if monitor:
        with open(path_html, "r") as f:
            stored_page = f.read()
        different = compare_pages(page, stored_page)

        if different:
            print("The pages are different!")
            send_mail()
        else:
            print("No differences found between the pages")

    pass


def store_page(page, path_html):
    with open(path_html, "w") as f:
        f.write(page)


def compare_pages(new_page, old_page):
    # TODO: smarter comparison, using BS?
    different = old_page == new_page
    return different


def send_mail():
    host = os.environ["SMTP_HOST"]
    port = os.environ["SMTP_PORT"]
    login = os.environ["SMTP_LOGIN"]
    to = os.environ["SMTP_PSW"]
    psw = os.environ["TO"]

    # set up the SMTP server
    s = smtplib.SMTP_SSL(host=host, port=port)
    s.login(login, psw)

    msg = MIMEMultipart()  # create a message

    # setup the parameters of the message
    msg["From"] = login
    msg["To"] = to
    msg["Subject"] = "Monitoring alert: website changed"

    msg.attach(MIMEText("The website you are monitoring changed", "plain"))

    s.send_message(msg)
    del msg

    # Terminate the SMTP session and close the connection
    s.quit()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "url",
        type=str,
        help="URL to monitor",
    )
    parser.add_argument(
        "path_html",
        type=str,
        help="Path to HTML file to compare/to store",
    )
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        "--monitor",
        action="store_true",
        help="Use this flag to monitor the URL againsgt saved page",
    )
    group.add_argument(
        "--store",
        action="store_true",
        help="Use this flag to save the page to the 'path_html' location",
    )

    args = parser.parse_args()

    main(**vars(args))