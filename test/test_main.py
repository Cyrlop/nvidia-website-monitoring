from nvidia_website_monitoring.main import compare_pages


def test_compare_pages():
    page_1 = "<html>A</html>"
    page_2 = "<html>B</html>"

    assert compare_pages(page_1, page_1) == True
    assert compare_pages(page_1, page_2) == False
