[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/python/black)
[![Python 3.7](https://img.shields.io/badge/python-3.7-blue.svg)](https://www.python.org/downloads/release/python-360/)

# NVidia monitoring

Quick script to monitor changes on the NVidia website

## Set-up
The follwing environment variables are required to set-up for the email notifications:

 * `SMTP_HOST`: SMTP host
 * `SMTP_PORT`: SMTP port
 * `SMTP_LOGIN`: SMTP login
 * `SMTP_PSW`: SMTP password
 * `TO`: Email address to send notifications to

## Usage

```
usage: main.py [-h] (--monitor | --store) url path_html

positional arguments:
  url         URL to monitor
  path_html   Path to HTML file to compare/to store

optional arguments:
  -h, --help  show this help message and exit
  --monitor   Use this flag to monitor the URL againsgt saved page
  --store     Use this flag to save the page to the 'path_html' location
```